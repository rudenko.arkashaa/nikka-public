use core::{mem, ops::Add};

use ku::error::{
    Error::{Medium, NoDisk},
    Result,
};

use super::{block_cache::BlockCache, BLOCK_SIZE, FIRST_BITMAP_BLOCK};

// Used in docs.
#[allow(unused)]
use ku::error::Error;


/// [Битмап](https://en.wikipedia.org/wiki/Free-space_bitmap)
/// для отслеживания какие именно блоки
/// [файловой системы](https://en.wikipedia.org/wiki/File_system) заняты, а какие свободны.
#[derive(Debug)]
pub(super) struct BlockBitmap {
    /// [Битмап](https://en.wikipedia.org/wiki/Free-space_bitmap),
    /// каждый элемент этого среза отвечает за [`Self::BITS_PER_ENTRY`] блоков.
    /// Сам срез хранится в памяти блочного кеша [`BlockCache`].
    bitmap: &'static mut [u64],

    /// Количество блоков в [файловой системе](https://en.wikipedia.org/wiki/File_system),
    /// то есть количество используемых бит в битмапе.
    block_count: usize,

    /// Место последней аллокации из битмапа.
    /// Служит для ускорения аллокаций и равномерного распределения занятых блоков по диску.
    cursor: usize,
}


impl BlockBitmap {
    /// Возвращает битмап файловой системы или ошибку [`Error::Medium`],
    /// если отведённое под битмап место диска содержит данные,
    /// которые не похожи на корректный битмап.
    pub(super) fn new(block_count: usize) -> Result<Self> {
        let block_bitmap = Self::new_unchecked(block_count);

        block_bitmap.validate()
    }


    /// Форматирует часть диска,
    /// отведённую под битмап для `block_count` блоков,
    /// его начальным состоянием.
    pub(super) fn format(block_count: usize) -> Result<()> {
        let mut block_bitmap = Self::new_unchecked(block_count);

        block_bitmap.bitmap.fill(0);

        for _ in 0..block_bitmap.reserved_block_count() {
            block_bitmap.allocate().unwrap();
        }

        if block_count < block_bitmap.bitmap.len() * Self::BITS_PER_ENTRY {
            block_bitmap.bitmap[block_count / Self::BITS_PER_ENTRY] =
                !0 << (block_count % Self::BITS_PER_ENTRY);
        }

        block_bitmap.validate()?;

        Ok(())
    }


    /// Возвращает `true`, если блок `number` свободен.
    ///
    /// # Panics
    ///
    /// Паникует, если `number` выходит за пределы диска --- [`BlockBitmap::block_count`].
    pub(super) fn is_free(&self, number: usize) -> bool {
        assert!(number < self.block_count);
        self.bitmap[number / Self::BITS_PER_ENTRY] & (1 << (number % Self::BITS_PER_ENTRY)) == 0
    }


    /// Помечает блок `number` как свободный.
    ///
    /// # Panics
    ///
    /// Паникует, если:
    ///   - Значение `number` выходит за пределы файловой системы --- [`BlockBitmap::block_count`].
    ///   - Блок `number` зарезервирован.
    ///   - Блок `number` уже помечен как свободный.
    pub(super) fn set_free(&mut self, number: usize) {
        assert!(self.reserved_block_count() <= number && number < self.block_count);
        assert!(!self.is_free(number));
        self.bitmap[number / Self::BITS_PER_ENTRY] &= !(1 << (number % Self::BITS_PER_ENTRY));
    }


    /// Находит по [`BlockBitmap::bitmap`] свободный блок и аллоцирует его.
    /// Возвращает номер выделенного блока или
    /// ошибку [`Error::NoDisk`], если свободных блоков не осталось.
    ///
    /// При поиске свободного блока стартует с номера [`BlockBitmap::cursor`] в
    /// срезе [`BlockBitmap::bitmap`] и запоминает в неё номер элемента,
    /// из которого выделен свободный блок.
    pub(super) fn allocate(&mut self) -> Result<usize> {
        // TODO: your code here.
        unimplemented!();
    }


    /// Возвращает количество свободных блоков в файловой системе.
    pub(super) fn free_block_count(&self) -> usize {
        self.bitmap
            .iter()
            .map(|bits| bits.count_zeros().try_into().unwrap())
            .reduce(Add::add)
            .unwrap_or(0)
    }


    /// Возвращает количество зарезервированных блоков в файловой системе.
    pub(super) fn reserved_block_count(&self) -> usize {
        FIRST_BITMAP_BLOCK + self.size_in_blocks()
    }


    /// Создаёт [`BlockBitmap`], не проверяя корректность его данных на диске.
    fn new_unchecked(block_count: usize) -> Self {
        Self {
            bitmap: Self::bitmap(block_count),
            block_count,
            cursor: 0,
        }
    }


    /// Проверяет корректность данных [`BlockBitmap`] на диске.
    /// Возвращает ошибку [`Error::Medium`],
    /// если данные на диске заведомо не содержат корректный [`BlockBitmap`],
    /// или сам [`BlockBitmap`] иначе.
    fn validate(self) -> Result<Self> {
        let bit_count = self.bitmap.len() * Self::BITS_PER_ENTRY;
        if bit_count < self.block_count || self.block_count < self.reserved_block_count() {
            return Err(Medium);
        }

        let exceding_blocks = self.block_count != self.bitmap.len() * Self::BITS_PER_ENTRY;
        if exceding_blocks {
            let exceding_blocks_expected_bitmap = !0 << (self.block_count % Self::BITS_PER_ENTRY);
            let exceding_blocks_actual_bitmap =
                self.bitmap[self.bitmap.len() - 1] & exceding_blocks_expected_bitmap;
            if exceding_blocks_actual_bitmap != exceding_blocks_expected_bitmap {
                return Err(Medium);
            }
        }

        if (0..self.reserved_block_count()).all(|reserved_block| !self.is_free(reserved_block)) {
            Ok(self)
        } else {
            Err(Medium)
        }
    }


    /// Возвращает количество блоков, которые занимает [`BlockBitmap`] на диске.
    fn size_in_blocks(&self) -> usize {
        mem::size_of_val(self.bitmap).div_ceil(BLOCK_SIZE)
    }


    /// Возвращает срез элементов битмапа в памяти блочного кеша [`BlockBitmap`].
    fn bitmap(block_count: usize) -> &'static mut [u64] {
        unsafe {
            BlockCache::block(FIRST_BITMAP_BLOCK)
                .unwrap()
                .start_address()
                .try_into_mut_slice::<u64>(block_count.div_ceil(Self::BITS_PER_ENTRY))
                .unwrap()
        }
    }


    /// Количество блоков, за которые отвечает один элемент среза [`BlockBitmap::bitmap`].
    const BITS_PER_ENTRY: usize = u64::BITS as usize;
}


impl Clone for BlockBitmap {
    fn clone(&self) -> Self {
        Self {
            bitmap: Self::bitmap(self.block_count),
            block_count: self.block_count,
            cursor: self.cursor,
        }
    }
}


#[doc(hidden)]
pub mod test_scaffolding {
    use ku::error::Result;


    pub struct BlockBitmap(pub(in super::super) super::BlockBitmap);


    impl BlockBitmap {
        pub fn new(block_count: usize) -> Result<Self> {
            Ok(Self(super::BlockBitmap::new(block_count)?))
        }


        pub fn format(block_count: usize) -> Result<()> {
            super::BlockBitmap::format(block_count)
        }


        pub fn is_free(&self, number: usize) -> bool {
            self.0.is_free(number)
        }


        pub fn set_free(&mut self, number: usize) {
            self.0.set_free(number)
        }


        pub fn allocate(&mut self) -> Result<usize> {
            self.0.allocate()
        }


        pub fn free_block_count(&self) -> usize {
            self.0.free_block_count()
        }
    }
}
